/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Examenes.PrimerParcial.L_P_1_2019.Lista;

import Examenes.PrimerParcial.*;
import ADT_Lista.*;

/**
 *
 * @author Ronaldo Rivero
 */
public class Nodo {
    private int dato;
    private Nodo enlace;

    public Nodo() {
        dato = -1;
        enlace = null;
    }

    public Nodo(int dato) {
        this.dato = dato;        
        enlace = null;
    }
    
    public Nodo(Nodo nodo) {
        dato = nodo.getDato();       
        enlace = nodo.getEnlace();
    }

    public int getDato() {
        return dato;
    }

    public void setDato(int dato) {
        this.dato = dato;
    }   

    public Nodo getEnlace() {
        return enlace;
    }

    public void setEnlace(Nodo enlace) {
        this.enlace = enlace;
    }
    
    
}
