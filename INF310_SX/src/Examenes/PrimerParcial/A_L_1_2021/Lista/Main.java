package Examenes.PrimerParcial.A_L_1_2021.Lista;

public class Main {    
    
    public static void main(String[] args) {
        Juego P = new Juego();
        
        boolean b = P.add(2, 9);
        System.out.print("P.add(9, 2) = ");
        System.out.println(P);
        System.out.println(b);
        
        b = P.add(3, 4);
        System.out.print("P.add(3, 4) = ");        
        System.out.println(P);
        System.out.println(b);
        
        b = P.add(4, 5);
        System.out.print("P.add(4, 5) = ");
        System.out.println(P);
        System.out.println(b);
    }
    
}
