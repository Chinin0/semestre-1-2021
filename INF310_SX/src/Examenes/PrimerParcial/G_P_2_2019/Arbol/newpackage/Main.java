package Examenes.PrimerParcial.G_P_2_2019.Arbol.newpackage;


public class Main {
    private static final int dataA[][]={{60,40,65},{40,30,50},{30,20,35},{50,45,55},{65,62,70},{70,68,80}}; 
    
    private static Arbol load(int v[][]){
        Arbol t=null;
        
        for (int i=0; i<v.length; i++){
            int padre = v[i][0];            //v[i] = {padre, hijoIzq, hijoDer}
            if (t==null)
                t = new Arbol(padre);
            
            for (int j=1; j<v[i].length; j++){
                t.add(padre, v[i][j]);
            }
        }
        return t;
    }
    
    public static void main(String[] args) {  
        Arbol A = load(dataA);      //Cargar los datos del Arbol A, mostrado en el examen.      
        
        A.niveles();
        
        A.cutLeafBunchs(62);
        A.niveles();
        A.cutLeafBunchs(150);
        A.niveles();
        A.cutLeafBunchs(80);
        A.niveles();
        A.cutLeafBunchs(20);
        A.niveles();
    }
    
}
