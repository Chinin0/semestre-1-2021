package Mesa_4_2021;

import java.util.ArrayList;
import java.util.List;

public class Nodo {
    
    private int M;
    private String dato;    
    private List<Nodo> hijos;
    
    public Nodo(int M, String dato) {
        this.M = M;
        this.dato = dato;
        this.hijos = new ArrayList<Nodo>();
                
        
    }
    
    public String getDato() {
        return dato;
    }
    
    public void setDato(String dato) {
        this.dato = dato;
    }
    
    public Nodo getHijo(int pos) {
        return hijos.get(pos);
    }
    
    public void setHijo(Nodo p, int i) {
        hijos.set(i, p);
    }
    
    public void setHijo(Nodo p) {
        hijos.add(p);
    }
        
    public int cantHijos() {
        return hijos.size();
    }
}
