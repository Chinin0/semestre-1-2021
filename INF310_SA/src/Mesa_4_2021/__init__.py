from Arbol import Arbol

arbol = Arbol(4)
arbol.insertar('root', None)
arbol.insertar('c:/', 'root')
arbol.insertar('Programas', 'c:/')
arbol.insertar('MisDoc', 'c:/')
arbol.insertar('Win32', 'c:/')
arbol.insertar('Python', 'Programas')
arbol.insertar('Java', 'Programas')
arbol.insertar('C++', 'Programas')
arbol.insertar('Imagenes', 'MisDoc')
arbol.insertar('img.bmp', 'Imagenes')

arbol.preOrden()