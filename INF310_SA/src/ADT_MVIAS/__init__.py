from Arbol import Arbol

arbol = Arbol(4)
arbol.insertar(100)
arbol.insertar(150)
arbol.insertar(200)

arbol.insertar(30)
arbol.insertar(60)
arbol.insertar(90)

arbol.insertar(20)

arbol.insertar(110)
arbol.insertar(120)
arbol.insertar(130)

arbol.insertar(160)

arbol.insertar(210)
arbol.insertar(211)
arbol.insertar(212)

print("Suma de datos = " + str(arbol.sumaNodo()))
print("Cantidad de nodos = " + str(arbol.cantNodos()))
print("Cantidad de nodos llenos = " + str(arbol.cantNodosLlenos()))
print("Cantidad de nodos escalera = " + str(arbol.cantNodosEscalera()))