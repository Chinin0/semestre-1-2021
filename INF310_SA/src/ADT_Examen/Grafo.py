# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
from ADT_Examen.Lista import Lista

class Grafo:
    __MAXVERTICE = 100
    __n = 0    
    __V = []
    __nombre = []
    __marca = []
    
    def __init__(self):
        self.__n = -1   
        self.__V = []
        self.__nombre = []
        self.__construirMarca()
        self.__desmarcarTodo()
        
    def agregarVertice(self, nombre):
        if self.__n < self.__MAXVERTICE:
            self.__V.append(Lista())
            self.__nombre.append(nombre)
            self.__n = self.__n + 1
        else:
            print("Error, no se puede agregar mas vertice")
    
    def agregarArista(self, nombreU, peso, nombreV):
        u = self.__nombre.index(nombreU)
        v = self.__nombre.index(nombreV)
        if self.__esValidoVertice(u) and self.__esValidoVertice(v):
            if peso < 0:
                print("Error, no se puede agregar un peso negativo")
                return
            self.__V[u].add(v, peso)
    
    def imprimir(self):        
        for x in range(self.__n+1):
            print(self.__nombre[x] + " -> " + self.__V[x].toString(self.__nombre))
    
    def dfs(self, u):
        self.__desmarcarTodo()
        print("DFS:")
        self.__dfs1(u)
        print("FIN::DFS")
        return
    
    def __dfs1(self, u):
        print(str(u))
        self.__marcar(u)
        for x in range(self.__V[u].length()):
            v = self.__V[u].get(x)
            if(self.__estaMarcado(v) == False):
                self.__dfs1(v)   
    
    def alcanzable(self, nombreU, nombreV):
        u = self.__nombre.index(nombreU)
        v = self.__nombre.index(nombreV)
        return self.alcanzable1(u, v)
        
    def alcanzable1(self, u, v):
        self.__marcar(u)
        minCosto = 99999
        minVert = -1
        for x in range(self.__V[u].length()):
            w = self.__V[u].get(x)
            if(w == v):
                return self.__V[u].getPeso(w) + 3
            
            if(self.__estaMarcado(w) == False):
                costo = self.alcanzable1(w, v)
                if ( costo > 0):
                    if(costo < minCosto):
                        minCosto = costo
                        minVert = w
        
        self.__desmarcar(u)
        if(minVert != -1):
            return minCosto + self.__V[u].getPeso(minVert) + 3
        return -1
        
    
    def __esValidoVertice(self, v):
        if v >= 0 and v <= self.__n:
            return True
        print("Error, vertice invalido")
        return False
    
    def __construirMarca(self):
        for x in range(self.__MAXVERTICE):
            self.__marca.append(False)
    
    def __desmarcarTodo(self):
        for x in range(self.__MAXVERTICE):
            self.__marca[x] = False
    
    def __marcarTodo(self):
        for x in range(self.__MAXVERTICE):
            self.__marca[x] = True
    
    def __marcar(self, x):
        if self.__esValidoVertice(x):
            self.__marca[x] = True
    
    def __desmarcar(self, x):
        if self.__esValidoVertice(x):
            self.__marca[x] = False      
       
    def __estaMarcado(self, x):
        return self.__marca[x]