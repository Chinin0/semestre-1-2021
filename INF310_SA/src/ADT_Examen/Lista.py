# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
from ADT_Examen.Nodo import Nodo

class Lista:
    __L = None
    __n = 0
    
    def __init__(self):
        self.__L = None
        self.__n = 0
       
    def add(self, dato, peso):
        ant = None
        p   = self.__L
        
        while (p is not None) and (dato >= p.getDato()):
            ant = p
            p = p.getLink()     
        
        if ant is None:
            nuevo = Nodo(dato, peso)
            nuevo.setLink(self.__L)
            self.__L = nuevo
            self.__n = self.__n + 1
        else:
            if ant.getDato() != dato:
                nuevo = Nodo(dato, peso)
                ant.setLink(nuevo)
                nuevo.setLink(p)
                self.__n = self.__n + 1
    
    def get(self, k):
        p = self.__L        
        i = 0
        while p is not None:
            if i == k:
                return p.getDato()
            p = p.getLink()
            i = i + 1    
        
        print("Lista.get: Fuera de rango");
        return -1
    
    def getPeso(self, dato):
        p = self.__exist(dato)
        if p is not None:
            return p.getPeso()
        return -1
    
    def length(self):
        return self.__n 
    
    def existe(self, dato):
        return (self.__exist(dato) is not None)    
    
    def __exist(self, dato):
        p = self.__L
        
        while (p is not None) and (dato > p.getDato()):
            p = p.getLink()
        
        if (p is not None) and (p.getDato() == dato):
            return p
        
        return None
        
    def toString(self, nombres):
        S = "["
        coma=""
       
        p  = self.__L;
        while p is not None:
            S += coma + nombres[p.getDato()] + "/" + str(p.getPeso())
            coma=", "
            p = p.getLink()       
        return S+"]"
