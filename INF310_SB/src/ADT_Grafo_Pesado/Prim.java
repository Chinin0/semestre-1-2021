/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_Grafo_Pesado;

import java.util.ArrayList;
import java.util.List;

public class Prim {
    public static final double INF = 63000.00;
    
    private List<List<AdyacentesConPeso>> adyacencia;
    private List<List<AdyacentesConPeso>> arbol;
    private List<Integer> marcado;
    private int N;
    
    public Prim(Grafos G) {
        N = G.cantidadDeVertices();
        adyacencia = G.listaDeAdyacencia;
        arbol = new ArrayList<>();
        marcado = new ArrayList<>();
        for(int i = 0; i < N; i++) {
            arbol.add(new ArrayList<AdyacentesConPeso>());            
        }
    }
    
    public void run() {
        int padre = 0;
        int hijo = 0;
        
        while(marcado.size() < N) {
            padre = hijo;
            
            marcado.add(padre);
            
            double min = INF;
            for(int i = 0; i < marcado.size(); i++) {
                int verticeI = marcado.get(i);
                for(int j = 0; j < adyacencia.get(verticeI).size(); j++) {
                    int verticeJ = adyacencia.get(verticeI).get(j).getIndiceVertice();
                    double pesoJ = adyacencia.get(verticeI).get(j).getPeso();
                    if(estaMarcado(verticeJ) == false) {
                        if(pesoJ < min) {
                            min = pesoJ;
                            padre = verticeI;
                            hijo = verticeJ;
                        } 
                    }                     
                }
            }
            if(min != INF) {
               arbol.get(padre).add(new AdyacentesConPeso(hijo, min));
               arbol.get(hijo).add(new AdyacentesConPeso(padre, min)); 
            }
            
        }
    }
    
    public Grafos getGrafoPrim() {
        Grafos gk = new Grafos();
        gk.listaDeAdyacencia = arbol;
        return gk;
    }
    
    
    public String mostrarArbolPrim() {
        String s = "";
        for(int i = 0; i < arbol.size(); i++) {
            for(int j = 0; j < arbol.get(i).size(); j++) {
                s = s + i + " - " + arbol.get(i).get(j).getIndiceVertice()
                        + " = " + arbol.get(i).get(j).getPeso() + "\n";
            }
        }
        return s;
    }
    
    private boolean estaMarcado(int v) {
        for(int i = 0; i < marcado.size(); i++) {
            if(marcado.get(i) == v) {
                return true;
            }
        }
        return false;
    }
}
